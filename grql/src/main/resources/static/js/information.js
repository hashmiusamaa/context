
var companyDataArray = [];
var companyData = {
    companyName:"Abengoa Bioenergy",
    companyHeadquarter:"St. Louis, MO, USA; Spain, Netherlands, Brazil",
    companyBusinessLocation:"globally",
    companyFeedstock:"wheat, barley, corn, sorghum, sugar cane, vegetable oil",
    companyGeneration:"First",
    companyYearFounded:"2003",
    companyBusinessType:"Production",
    companyTechnology:"Fermentation, Transesterification",
    companyPrimaryProduct:"bioethanol, biodiesel, cellulosic fuel	feed, alt fuels"
};
companyDataArray.push(companyData);

    data = companyDataArray[0];
//    console.log(data);
    for(item in data){
//        console.log(item + "\t" + data[item]);
        $('#'+item).text(data[item]);
    }
