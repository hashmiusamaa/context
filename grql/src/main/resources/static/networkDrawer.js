// Code taken from internet, should not be considered part of the application.
var nodes, edges, network, nodesQuery, edgesQuery, networkQuery, graph;

// convenience method to stringify a JSON object
function toJSON(obj) {
    return JSON.stringify(obj, null, 4);
}

function addNode() {
    try {
        nodes.add({
            id: document.getElementById('node-id').value,
            label: document.getElementById('node-id').value,
            properties: JSON.parse(document.getElementById('node-properties').value)
        });
    }
    catch (err) {
        alert(err);
    }
}

function updateNode() {
    try {
        nodes.update({
            id: document.getElementById('node-id').value,
            label: document.getElementById('node-id').value,
            properties: JSON.parse(document.getElementById('node-properties').value)

        });
    }
    catch (err) {
        alert(err);
    }
}
function removeNode() {
    try {
        nodes.remove({id: document.getElementById('node-id').value});
    }
    catch (err) {
        alert(err);
    }
}

function addEdge() {
    try {
        edges.add({
            id: document.getElementById('edge-id').value,
            label: document.getElementById('edge-id').value,
            from: document.getElementById('edge-from').value,
            to: document.getElementById('edge-to').value,
            properties: JSON.parse(document.getElementById('edge-properties').value)

        });
    }
    catch (err) {
        alert(err);
    }
}
function updateEdge() {
    try {
        edges.update({
            id: document.getElementById('edge-id').value,
            from: document.getElementById('edge-from').value,
            to: document.getElementById('edge-to').value,
            properties: JSON.parse(document.getElementById('edge-properties').value)
        });
    }
    catch (err) {
        alert(err);
    }
}
function removeEdge() {
    try {
        edges.remove({id: document.getElementById('edge-id').value});
    }
    catch (err) {
        alert(err);
    }
}

function draw() {
    // create an array with nodes
    nodes = new vis.DataSet();
    nodesQuery = new vis.DataSet();
    nodes.on('*', function () {
        document.getElementById('nodes').innerHTML = JSON.stringify(nodes.get(), null, 4);
    });
/*    nodes.add([
        {
            "id": "us",
            "label": "us",
            "properties": {}
        },
        {
            "id": "usb",
            "label": "usb",
            "properties": {}
        },
        {
            "id": "cde",
            "label": "cde",
            "properties": {}
        },
        {
            "id": "usc",
            "label": "usc",
            "properties": {
                "age": 32
            }
        },
        {
            "id": "uscd",
            "label": "uscd",
            "properties": {
                "age": 31
            }
        }
    ]);*/

    // create an array with edges
    edges = new vis.DataSet();
    edgesQuery = new vis.DataSet();
    edges.on('*', function () {
        document.getElementById('edges').innerHTML = JSON.stringify(edges.get(), null, 4);
    });
/*    edges.add([
        {
            "id": "ad",
            "label": "ad",
            "from": "us",
            "to": "usb",
            "properties": {}
        },
        {
            "id": "bc",
            "label": "bc",
            "from": "usb",
            "to": "cde",
            "properties": {}
        },
        {
            "id": "aga",
            "label": "aga",
            "from": "usb",
            "to": "usc",
            "properties": {
                "count": 200
            }
        },
        {
            "id": "aga2",
            "label": "aga2",
            "from": "usb",
            "to": "uscd",
            "properties": {
                "count": 200
            }
        }
    ]);

    // create a network
/    var container = document.getElementById('network');
    var data = {
        nodes: nodes,
        edges: edges
    };
    graph = data;
    var options = {};
    network = new vis.Network(container, data, options);
*/
}

$(document).ready(function(){
    $("#runQuery").on("click", function() {
        postUsernameToServer();

    });

});



function postUsernameToServer() {
    var query = $("#queryText").val();
    graph = {nodes:{}, edges:{}};
//    mynodes = nodes.get();
//    myedges = edges.get();
	mynodes = JSON.parse(document.getElementById("nodes").innerHTML)
	myedges = JSON.parse(document.getElementById("edges").innerHTML)
//	console.log(document.getElementById("nodes").innerHTML)
    for(node in mynodes){
        delete mynodes[node]['id']
        graph.nodes[mynodes[node].label] = mynodes[node];
	console.log("node" + node);
    }
    for(edge in myedges){
        delete myedges[edge]['id']
        graph.edges[myedges[edge].label] = myedges[edge];
	console.log("edge" + edge);
    }
	console.log("sending this graph: "+JSON.stringify(graph))
    $.ajax({
        url: "http://localhost:8080/query",
        type: "POST",
        data: {query: query, graph: JSON.stringify(graph)},
        success: function(returned) {
		console.log("sent graph" + JSON.stringify(graph));
            	returnedGraph = JSON.parse(returned).graph;
		console.log("Returned: "+returnedGraph);
            	returnedGraph1 = convertData(JSON.parse(returnedGraph));
//            console.log(returnedGraph1);
            nodesQuery = new vis.DataSet();
            edgesQuery = new vis.DataSet();
            nodesQuery.on('*', function () {
                document.getElementById('nodesQuery').innerHTML = JSON.stringify(nodesQuery.get(), null, 4);
            });
            edgesQuery.on('*', function () {
                document.getElementById('edgesQuery').innerHTML = JSON.stringify(edgesQuery.get(), null, 4);
            });
            nodesQuery.add(returnedGraph1.nodes);
            edgesQuery.add(returnedGraph1.edges);
            var container = document.getElementById('networkQuery');
            var data1 = {
                nodes: nodesQuery,
                edges: edgesQuery
            };

            var options = {};
            networkQuery = new vis.Network(container, data1, options);
            document.getElementById("intermediary").innerText =
                JSON.stringify(JSON.parse(returned).intermediateCode);

        },
        error: function(e) {
            alert(JSON.stringify(e));
        }
    });

    function convertData(data) {
        for(node in data.nodes){
            data.nodes[node]['id'] = data.nodes[node].label;
        }
        for(edge in data.edges){
            data.edges[edge]['id'] = data.edges[edge].label;
        }
        console.log(data);
        return data;
    }


}
