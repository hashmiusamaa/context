package com.hashmiusama.grql.graph;

import com.google.gson.Gson;
import com.hashmiusama.grql.generics.Generics;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;
import java.util.*;

public class Graph implements Serializable{
    // The list in each node is a list of integers telling which node at which integer is connected to this node
    public Map<String, Node> nodes = new HashMap<>();
    public Map<String, Edge> edges = new HashMap<>();

    // Adds edge to the edges List
    public void addEdge(String label, String from, String to, Map<String, Object> properties){
        Edge edge = new Edge(label, from, to, properties);
        edges.put(label, edge);
    }

    public Map<String, Edge> getEdgesByPropertyValue(String key, Object value) {
        Map<String, Edge> outputEdges = new HashMap<>();
        for (Edge edge : edges.values()) {
            if (key.equals("label")) {
                System.out.println("Label case " + edge.label + " " + value);
                if (value.equals("*")) {
                    outputEdges.put(edge.label, edge);
                } else if (value.equals(edge.label)) {
                    outputEdges.put(edge.label, edge);
//                    System.out.println("FOUND node: "+edge);
                }
            }
        }
        return outputEdges;
    }

    //aimed at searching for a node with given properties
    public Map<String, Edge> findEdgeWithProperties(Map<String, Object> properties){
        if(properties.size() == 0) return null;
        Map<String, Edge> found = new HashMap<>();
        found.putAll(edges);
        for(Map.Entry<String, Object> entry: properties.entrySet()){
            found.keySet().retainAll(getNodesByPropertyValue(entry.getKey(), entry.getValue()).keySet());
        }
        return found;
    }

    public Map<String, Edge> findEdgeWithoutProperties(Map<String, Object> properties){
        Graph graph = new Graph();
        graph.edges.keySet().removeAll(findEdgeWithProperties(properties).keySet());
        return graph.edges;
    }

    public void addEdge(Edge edge){
        edges.put(edge.label, edge);
    }

    public Edge getEdge(String label){
        return edges.get(label);
    }

    public Map<String, Edge> getEdgesByInequality(String key, double value, String operator){
        Map<String, Edge> edgesFound = new HashMap<>();
        for (Edge edge : edges.values()) {
//            System.out.println(edge);
            if (edge.properties != null) {
                if (operator.equals(">")) {
                    if (edge.properties.get(key) != null) {
                        if ((double) edge.properties.get(key) > value) {
                            edgesFound.put(edge.label, edge);
                        }
                    } else if (operator.equals(">=")) {
                        if ((double) edge.properties.get(key) >= value) {
                            edgesFound.put(edge.label, edge);
                        }
                    } else if (operator.equals("<")) {
                        if ((double) edge.properties.get(key) < value) {
                            edgesFound.put(edge.label, edge);
                        }
                    } else if (operator.equals("<=")) {
                        if ((double) edge.properties.get(key) <= value) {
                            edgesFound.put(edge.label, edge);
                        }
                    }
                }
            }
        }
        return edgesFound;
    }

    //this function recursively checks for a property and returns true if a property is found
    private boolean searchMap(Map<String, Object> map, String key, Object value){
        for(Map.Entry entry: map.entrySet()){
            if(entry.getValue() instanceof Map) return searchMap((Map<String, Object>) entry.getValue(), key, value);
            if (entry.getKey().equals(key)){
                if (entry.getValue().equals(value)){
                    return true;
                }
            }
        }
        return false;
    }

    // increases count and add another node to the graph without any properties
    public void addNode(String label, Map<String, Object> properties){
        if (nodes.get(label) != null){
            nodes.replace(label, new Node(label, properties));
        }else{
            nodes.put(label, new Node(label, properties));
        }
    }

    public void addNode(Node node){
        if (nodes.containsKey(node.label)){
            nodes.get(node.label).properties.putAll(node.properties);
        }else{
            nodes.put(node.label, node);
        }
    }

    public Node getNode(String label){
        return nodes.get(label);
    }

    //this function calls the searchMap function for each node and returns a set of nodes that has
    // desired key and value.
    public Map<String, Node> getNodesByPropertyValue(String key, Object value){
        Map<String, Node> outputNodes = new HashMap<>();
        for(Node node: nodes.values()){
            if(key.equals("label")){
                System.out.println("Label case " + node.label + " " + value);
                if(value.equals("*")){
                    outputNodes.put(node.label, node);
                }else
                if(value.equals(node.label)) {
                    outputNodes.put(node.label, node);
                    System.out.println("FOUND node: "+node);
                }
            }else if(searchMap(node.properties, key, value)){
                outputNodes.put(node.label, node);
            }
        }
        return outputNodes;
    }

    //aimed at searching for a node with given properties
    public Map<String, Node> findNodeWithProperties(Map<String, Object> properties){
        if(properties.size() == 0) return null;
        Map<String, Node> found = new HashMap<>();
        found.putAll(nodes);
        for(Map.Entry<String, Object> entry: properties.entrySet()){
            found.keySet().retainAll(getNodesByPropertyValue(entry.getKey(), entry.getValue()).keySet());
        }
        return found;
    }

    public Map<String, Node> findNodeWithoutProperties(Map<String, Object> properties){
        Graph graph = new Graph();
        graph.nodes.keySet().removeAll(findNodeWithProperties(properties).keySet());
        return graph.nodes;
    }

    public Map<String, Node> getNodesByInequality(String key, double value, String operator){
        Map<String, Node> edgesFound = new HashMap<>();
        for (Node node : nodes.values()) {
            System.out.println(node);
            if (node.properties != null) {
                if (node.properties.get(key) != null) {
                    if (operator.equals(">")) {
                        if ((double) node.properties.get(key) > value) {
                            edgesFound.put(node.label, node);
                        }
                    } else if (operator.equals(">=")) {
                        if ((double) node.properties.get(key) >= value) {
                            edgesFound.put(node.label, node);
                        }
                    } else if (operator.equals("<")) {
                        if ((double) node.properties.get(key) < value) {
                            edgesFound.put(node.label, node);
                        }
                    } else if (operator.equals("<=")) {
                        if ((double) node.properties.get(key) <= value) {
                            edgesFound.put(node.label, node);
                        }
                    }
                }
            }
        }
        return edgesFound;
    }

    public String displayGraph(){
        Map<String, Object> output = new HashMap<>();
        output.put("nodes", nodes.values());
        output.put("edges", edges.values());
        return new Gson().toJson(output);
    }

    @Override
    public String toString() {
        return "{\"nodes\":" + nodes +
                ",\"edges\":"+ edges +
                '}';
    }
/*
    public static void main(String[] args) throws IOException {
        Graph graph = new Graph();
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("name", "Usama");
        properties.put("age", 5);

        properties = (HashMap<String, Object>) Generics.doubler(properties);
        graph.addNode("Usama", properties);
        properties = (HashMap<String, Object>) Generics.doubler(properties);
        properties.put("name", "Random");
        graph.addNode("Random", properties);
        graph.addEdge("FriendOf", "Usama", "Random", properties);

        System.out.println(graph.getEdgesByInequality("age", 1, ">"));
        System.out.println(graph);
    }*/
}
