// Generated from C:/Users/hashmiusama/IdeaProjects/grql/src/main/java/com/hashmiusama/grammar\GrQL.g4 by ANTLR 4.7
package com.hashmiusama.grql.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrQLParser}.
 */
public interface GrQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrQLParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(GrQLParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(GrQLParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#entity}.
	 * @param ctx the parse tree
	 */
	void enterEntity(GrQLParser.EntityContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#entity}.
	 * @param ctx the parse tree
	 */
	void exitEntity(GrQLParser.EntityContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#entities}.
	 * @param ctx the parse tree
	 */
	void enterEntities(GrQLParser.EntitiesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#entities}.
	 * @param ctx the parse tree
	 */
	void exitEntities(GrQLParser.EntitiesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#graph}.
	 * @param ctx the parse tree
	 */
	void enterGraph(GrQLParser.GraphContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#graph}.
	 * @param ctx the parse tree
	 */
	void exitGraph(GrQLParser.GraphContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#node}.
	 * @param ctx the parse tree
	 */
	void enterNode(GrQLParser.NodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#node}.
	 * @param ctx the parse tree
	 */
	void exitNode(GrQLParser.NodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#edge}.
	 * @param ctx the parse tree
	 */
	void enterEdge(GrQLParser.EdgeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#edge}.
	 * @param ctx the parse tree
	 */
	void exitEdge(GrQLParser.EdgeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#pairs}.
	 * @param ctx the parse tree
	 */
	void enterPairs(GrQLParser.PairsContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#pairs}.
	 * @param ctx the parse tree
	 */
	void exitPairs(GrQLParser.PairsContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(GrQLParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(GrQLParser.PairContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(GrQLParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(GrQLParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#keyexpression}.
	 * @param ctx the parse tree
	 */
	void enterKeyexpression(GrQLParser.KeyexpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#keyexpression}.
	 * @param ctx the parse tree
	 */
	void exitKeyexpression(GrQLParser.KeyexpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#valueexpression}.
	 * @param ctx the parse tree
	 */
	void enterValueexpression(GrQLParser.ValueexpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#valueexpression}.
	 * @param ctx the parse tree
	 */
	void exitValueexpression(GrQLParser.ValueexpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#valueexpressions}.
	 * @param ctx the parse tree
	 */
	void enterValueexpressions(GrQLParser.ValueexpressionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#valueexpressions}.
	 * @param ctx the parse tree
	 */
	void exitValueexpressions(GrQLParser.ValueexpressionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#key}.
	 * @param ctx the parse tree
	 */
	void enterKey(GrQLParser.KeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#key}.
	 * @param ctx the parse tree
	 */
	void exitKey(GrQLParser.KeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#val}.
	 * @param ctx the parse tree
	 */
	void enterVal(GrQLParser.ValContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#val}.
	 * @param ctx the parse tree
	 */
	void exitVal(GrQLParser.ValContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#conditions}.
	 * @param ctx the parse tree
	 */
	void enterConditions(GrQLParser.ConditionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#conditions}.
	 * @param ctx the parse tree
	 */
	void exitConditions(GrQLParser.ConditionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#valallowed}.
	 * @param ctx the parse tree
	 */
	void enterValallowed(GrQLParser.ValallowedContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#valallowed}.
	 * @param ctx the parse tree
	 */
	void exitValallowed(GrQLParser.ValallowedContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrQLParser#operator}.
	 * @param ctx the parse tree
	 */
	void enterOperator(GrQLParser.OperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrQLParser#operator}.
	 * @param ctx the parse tree
	 */
	void exitOperator(GrQLParser.OperatorContext ctx);
}
