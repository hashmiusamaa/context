package com.hashmiusama.grql.controllers;

import com.google.gson.Gson;
import com.hashmiusama.grql.generics.Generics;
import com.hashmiusama.grql.graph.Graph;
import com.hashmiusama.grql.intermediator.GrQLCompiler;
import com.hashmiusama.grql.intermediator.Gri;
import com.hashmiusama.grql.runtime.RuntimeEnvironment;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@RestController
public class QueryController {

    static GrQLCompiler compiler;
    static RuntimeEnvironment rtE;
// select [a] from [$a="label",@a="us"];
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public String queryController(@RequestParam String query, @RequestParam String graph) {
        //System.out.println(graph);
        compiler = new GrQLCompiler();
	System.out.println("graph from browser: " + graph);
        Graph input = Generics.fromJsonToGraph(graph);
        System.out.println(input);
        try {
            query = URLDecoder.decode(query,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(query);

        rtE = new RuntimeEnvironment();
        rtE.gri = compiler.compile(query);
        rtE.graph = input;
        Graph returnGraph = rtE.runtimeExecute();
        System.out.println(Generics.fromGriToJson(rtE.gri));
        System.out.println(rtE.gri);
        System.out.println(returnGraph);
        Map<String, Object> returningObject = new HashMap<>();
        returningObject.put("graph", returnGraph.displayGraph());
        returningObject.put("intermediateCode", rtE.gri);
        return new Gson().toJson(returningObject);
    }
}
